#!/usr/bin/env bash

CLEAR="\033[0m\n"
REPEATED_TEXT="liOn, or 1ı0ń.\n !ïÓŋ. or liOn, ?"

echo Default color
printf "\033[1;37m $REPEATED_TEXT White $CLEAR"
printf "\033[0;37m $REPEATED_TEXT Light gray $CLEAR"
printf "\033[1;30m $REPEATED_TEXT Dark gray $CLEAR"
printf "\033[1;31m $REPEATED_TEXT Dark red $CLEAR"
printf "\033[0;31m $REPEATED_TEXT Light red $CLEAR"
echo Again default color

printf "\033[1;30m"
cat <<TABLE
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        7                                                                                │
  │ Passing:      4                                                                                │
  │ Failing:      0                                                                                │
  │ Pending:      2                                                                                │
  │ Skipped:      1                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     21 seconds                                                                       │
  │ Spec Ran:     my-cypress-tests/my-cypress-test.spec.ts                                         │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
TABLE
printf $CLEAR
echo More default color
echo Even more default color
